package com.safebear.app;

import org.junit.Test;

import static org.junit.Assert.assertTrue;


public class Test01_Login extends BaseTest {

    @Test
    public void testLogin(){

        //1. Confirm on Home
        assertTrue(homePage.checkPage());
        //2. clicking login link in main navigation
        homePage.clickLoginLink();
        //3. Confirm on Login
        assertTrue(loginPage.checkPage());
        //4. Login
        loginPage.login("testuser","testing");
        //5. confirm on Userpage
        assertTrue(userPage.checkPage());

    }

}
