package Demo;

public class Door {
    double width;
    double height;
    double depth;
    String colour;
    Boolean hasletterbox;
    String state;


public Door(double width, double height){
    width = width;
    this.height = height;
    state = "closed";
}



    public String getState(){
        return state;
    }

    public void setState(String newstate) {
        state = newstate;
    }

    public String getColour(){
        return colour;
    }
}
