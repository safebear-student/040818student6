package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;


public class LoginPage {
    WebDriver driver;

    @FindBy(id = "myid")
    WebElement user_field;

    @FindBy(xpath = "//*[contains(@id, 'mypass')]")
    WebElement pass_field;

    @FindAll({@FindBy(xpath = "//*[contains(@id, 'my')]")})
    List<WebElement> input_fields;


    public LoginPage(WebDriver driver){
        this.driver=driver;
        PageFactory.initElements(driver,this);
    }

    public boolean checkPage(){
        return driver.getTitle().contains("Sign In");
    }

    public void login(String username, String password){
        user_field.sendKeys(username);
        pass_field.sendKeys(password);
        pass_field.submit();
    }

    public void setInput_fields(String test){

    }
}
